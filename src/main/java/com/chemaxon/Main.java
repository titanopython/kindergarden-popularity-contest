package com.chemaxon;

import java.io.*;

public class Main {

    public static void main(String[] args) {

        KinderGardenPopularityCalculator kinderGardenPopularityCalculator = new KinderGardenPopularityCalculator();
        String fileName = "src\\main\\resources\\voting1";

        try {
            File file =  new File(fileName);
            InputStream inputStream = new FileInputStream(file);

            System.out.println(kinderGardenPopularityCalculator.getMostPopularKids(inputStream));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
