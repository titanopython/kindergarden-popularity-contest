package com.chemaxon;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class KinderGardenPopularityCalculator {

    public List<String> getMostPopularKids(InputStream inputStream) {

        List<String> votesWithVoters = processInputStream(inputStream);

        Map<String, Set<String>> voterToVotes = filterInValidVotes(votesWithVoters);

        Map<String, Integer> candidatesToVotes = countVotes(voterToVotes);

        List<String> winners = getWinner(candidatesToVotes);

        return winners;
    }

    List<String> processInputStream(final InputStream inputStream) {
        List<String> listOfBulkVotes = new ArrayList<>();
        int currentCharacterIntValue = 0;
        char currentCharacter;
        StringBuilder currentWord = new StringBuilder();

        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        Reader reader = new InputStreamReader(bufferedInputStream, StandardCharsets.UTF_8)){

            while (currentCharacterIntValue != -1) {
                currentCharacterIntValue = reader.read();
                currentCharacter = (char) currentCharacterIntValue;
                if (currentCharacter == '\r') {
                    listOfBulkVotes.add(currentWord.toString());
                    currentWord = new StringBuilder();
                    continue;
                }
                currentWord.append(currentCharacter);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listOfBulkVotes;
    }

    List<String> getWinner(final Map<String, Integer> candidateToVotes) {
        Integer highestVote = 0;
        List<String> winners = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : candidateToVotes.entrySet()) {
            if (entry.getValue() > highestVote) {
                highestVote = entry.getValue();
            }
            if (entry.getValue().equals(highestVote)) {
                winners.add(entry.getKey());
            }
        }
        for (int i = winners.size() - 1; i >= 0; i--) {
            if (candidateToVotes.get(winners.get(i)) < highestVote) {
                winners.remove(winners.get(i));
            }
        }
        return winners;
    }

    Map<String, Integer> countVotes(final Map<String, Set<String>> voterToVotes) {
        final Map<String, Integer> candidatesToVotes = new HashMap<>();
        for (Map.Entry<String, Set<String>> entry : voterToVotes.entrySet()) {
            for (String vote : entry.getValue()) {
                if (candidatesToVotes.get(vote) == null) {
                    candidatesToVotes.put(vote, 1);
                } else {
                    Integer currentVoteNumber = candidatesToVotes.get(vote);
                    candidatesToVotes.put(vote, ++currentVoteNumber);
                }
            }
        }
        return candidatesToVotes;
    }

    Map<String, Set<String>> filterInValidVotes(final List<String> votingList) {
        final Map<String, Set<String>> votersToVotes = new HashMap<>();
        for (String line : votingList) {
            List<String> names = Arrays.asList(line.split(",", -1));
            String voter = names.get(0);
            votersToVotes.putIfAbsent(voter, new HashSet<>());
            for (int i = 1; i < names.size(); i++) {
                if (!voter.equals(names.get(i))) {
                    votersToVotes.get(voter).add(names.get(i));
                }
            }
        }
        return votersToVotes;
    }
}
