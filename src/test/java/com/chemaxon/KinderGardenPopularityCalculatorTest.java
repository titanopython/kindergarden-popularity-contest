package com.chemaxon;

import org.junit.Test;

import java.io.*;
import java.util.*;

import static org.junit.Assert.*;

public class KinderGardenPopularityCalculatorTest {

    private final KinderGardenPopularityCalculator kinderGardenPopularityCalculator = new KinderGardenPopularityCalculator();

    @Test
    public void testGetWinnersWithMultipleWinnersWhenReadFromFile() {
        String fileName = "src\\main\\resources\\voting1";

        List<String> winners = new ArrayList<>();
        winners.add("Maria");
        winners.add("Margit");

        File file = new File(fileName);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        assertEquals(winners, kinderGardenPopularityCalculator.getMostPopularKids(inputStream));
    }

    @Test
    public void testGetWinnersWhenWinnerNameContainsCharCodeGreaterThan255Bytes() {
        String votes = "Mária,Irén,Margit" + '\r' +
                "Gerzson,Gerzson,Gerzson,Gerzson,Üllő" + '\r' +
                "Irén,Mária,Margit" + '\r' +
                "Gusztáv,Margit,Erzsébet,Walter" + '\r' +
                "Margit,Mária,Irén,Üllő" + '\r' +
                "Erzsébet,Ödön,Üllő" + '\r' +
                "Armand,Mária,Irén,Erzsébet,Franciska,Margit" + '\r' +
                "Walter,Mária" + '\r' +
                "Franciska,Gerzson,Üllő" + '\r' +
                "Ödön,Erzsébet" + '\r' +
                "Gerzson,Gerzson" + '\r' +
                "Ödön,Erzsébet,Üllő" + '\r' +
                "Erzsébet,Ödön";

        List<String> winners = new ArrayList<>();
        winners.add("Üllő");

        InputStream inputStream = new ByteArrayInputStream(votes.getBytes());

        assertEquals(winners, kinderGardenPopularityCalculator.getMostPopularKids(inputStream));
    }

    @Test
    public void filterInValidVotesWithDuplicateVotesTest() {
        List<String> votesWithVoters = new ArrayList<>();
        votesWithVoters.add("Maria,Margit,Geza");
        votesWithVoters.add("Maria,Geza");
        votesWithVoters.add("Margit,Geza");
        votesWithVoters.add("Margit,Hunor");

        Set<String> mariasVotes = new HashSet<>();
        mariasVotes.add("Margit");
        mariasVotes.add("Geza");
        Set<String> margitsVotes = new HashSet<>();
        margitsVotes.add("Geza");
        margitsVotes.add("Hunor");

        Map<String, Set<String>> voterToVotes = new HashMap<>();
        voterToVotes.put("Maria", mariasVotes);
        voterToVotes.put("Margit", margitsVotes);

        assertEquals(voterToVotes, kinderGardenPopularityCalculator.filterInValidVotes(votesWithVoters));
    }

    @Test
    public void filterInValidVotesWithEmptyListParameterTest() {
        List<String> votesWithVoters = new ArrayList<>();

        Map<String, Set<String>> voterToVotes = new HashMap<>();

        assertEquals(voterToVotes, kinderGardenPopularityCalculator.filterInValidVotes(votesWithVoters));
    }

    @Test
    public void filterInValidVotesWithOneVoterThatSubmitsAnEmptyListTest() {
        List<String> votesWithVoters = new ArrayList<>();
        votesWithVoters.add("Maria");

        Set<String> mariasVotes = new HashSet<>();

        Map<String, Set<String>> voterToVotes = new HashMap<>();
        voterToVotes.put("Maria", mariasVotes);

        assertEquals(voterToVotes, kinderGardenPopularityCalculator.filterInValidVotes(votesWithVoters));
    }

    @Test
    public void testCountVotesWhenNoDuplicateOrSelfSubmittedVotes() {
        Set<String> mariasVotes = new HashSet<>();
        mariasVotes.add("Margit");
        mariasVotes.add("Geza");
        Set<String> margitsVotes = new HashSet<>();
        margitsVotes.add("Geza");
        margitsVotes.add("Hunor");

        Map<String, Set<String>> voterToVotes = new HashMap<>();
        voterToVotes.put("Maria", mariasVotes);
        voterToVotes.put("Margit", margitsVotes);

        Map<String, Integer> candidatesToVotes = new HashMap<>();
        candidatesToVotes.put("Geza", 2);
        candidatesToVotes.put("Margit", 1);
        candidatesToVotes.put("Hunor", 1);

        assertEquals(candidatesToVotes, kinderGardenPopularityCalculator.countVotes(voterToVotes));

    }

    @Test
    public void testCountVotesWhenPassedAnEmptyMap() {
        Map<String, Integer> candidatesToVotes = new HashMap<>();
        Map<String, Set<String>> voterToVotes = new HashMap<>();

        assertEquals(candidatesToVotes, kinderGardenPopularityCalculator.countVotes(voterToVotes));
    }

    @Test
    public void testGetWinnersWhenThereIsOneWinner() {
        Map<String, Integer> candidatesToVotes = new HashMap<>();
        candidatesToVotes.put("Geza", 2);
        candidatesToVotes.put("Margit", 1);
        candidatesToVotes.put("Hunor", 1);

        List<String> winners = new ArrayList<>();
        winners.add("Geza");

        assertEquals(winners, kinderGardenPopularityCalculator.getWinner(candidatesToVotes));
    }

    @Test
    public void testGetWinnersWhenThereAreTwoWinners() {
        Map<String, Integer> candidatesToVotes = new HashMap<>();
        candidatesToVotes.put("Geza", 2);
        candidatesToVotes.put("Margit", 2);
        candidatesToVotes.put("Hunor", 1);

        List<String> winners = new ArrayList<>();
        winners.add("Geza");
        winners.add("Margit");

        assertEquals(winners, kinderGardenPopularityCalculator.getWinner(candidatesToVotes));
    }

    @Test
    public void testGetWinnersWhenPassedAnEmptyMap() {
        Map<String, Integer> candidatesToVotes = new HashMap<>();

        List<String> winners = new ArrayList<>();

        assertEquals(winners, kinderGardenPopularityCalculator.getWinner(candidatesToVotes));
    }

}
